package com.rtest.rcom.mygitapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Toast.makeText(getApplicationContext(),"First Change",Toast.LENGTH_SHORT).show();//Commenting By PC2
        Toast.makeText(getApplicationContext(), "SmallChange on FeatureBranch - By PC2", Toast.LENGTH_SHORT).show();

        findViewById(R.id.launchSecond).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SecondActivity.class));
            }
        });
    }
}
